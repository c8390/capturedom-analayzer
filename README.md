# CaptureDOM Analyzer



## Getting started

Before running the analysis is needed to run the XSS detection tools.
```shell
## chose XSS Tools folder **
cd XSS_Tools
### on Linux/macOS/Windows ###
docker-compose up --build
```

After start the Tools containse, is possible run the Python parser

Create python virtual environment
```shell
### on Windows ###
python -m venv venv
### on Linux/macOS ###
python3 -m venv venv
```
Activate the virtual environment
```shell
### on Windows ###
./venv/Scripts/activate
### on Linux/macOS ###
source ./venv/bin/activate
```

Install all dependencies in requirements.txt
```shell
pip install -r requirements.txt
```

Run the parser
```shell
python main.py
```

